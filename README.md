# port-authority

We use this repository to initiate deployments to `subway` and `high-line`.

## How to deploy either application

### 1. Setup
- Grab the commit SHA and branch name of the commit you want deployed 
- Create a new `port-authority` branch off of main. 
  - There is no enforced naming convention, but something like `rollout-<branch_name>-to-<application>-<environment>` works fine
- Navigate to the folder of the application you want to deploy, i.e. `/subway` or `/high-line`
- Open the file named `a8r-values-<env>.yml`, where `env` is the environment you want to deploy to

### 2. Update values
- Update the value for `ambassador.rollouts.image.repository` to match your branch name
  - For example, for `main`, this should be `registry.gitlab.com/upchieve/application/high-line/main`
  - **NOTE:** If your branch name has capital letters, change them to lowercase here!
- Update the value for `ambassador.rollouts.image.tag` to your commit SHA.
  - **NOTE:** For high-line deployments only, also append the environment name after the SHA like so:
    `2827f9ca5d1c98c8ace54146e9198531daf9764b-staging`
- Open an MR with these changes

### 3. Deploying
- Approve and merge in your MR
  - Once you do this, the next time Argo CD runs the sync job (which it does every few minutes), it will deploy the new image you specified. So make sure you only merge this into main once you're sure you're ready to go!
- Log into ArgoCD and navigate to Applications -> the application & environment you just updated the values for, i.e. `subway-staging` or `high-line-staging`
- You can wait for ArgoCD to pick up the changes automatically (which could take a while), or just go ahead and click `Refresh` in the UI
  - This will cause ArgoCD to detect the changes and then attempt to apply them
- Monitor the deployment.

### 4. Monitoring the deployment
- Watch as the old pods go down and new ones come up
- If any of the pods are unhealthy, they will be marked with a 💔 broken heart 💔 symbol. Click into the pod to see the errors and ping the group if you need any help resolving them.